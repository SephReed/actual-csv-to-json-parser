import * as fs from "fs";
import { promisify } from "util";

function readFile(path: string) {
  promisify(fs.readFile)(path);
}

function writeFile(path: string, data: string) {
  promisify(fs.writeFile)(path, data);
}


export interface ICsvParserArgs {
  delimiter?: RegExp | string;
  jsonTabSize?: null | number;
  trimRawText?: boolean;
  convertPrimitives?: boolean | Array<"boolean" | "number" | "json">,
}

/** Handles csv to json parsing, as well as a few other similar functionalities */
export class CsvParser {
  protected splitFinder: RegExp;

  constructor(protected args: ICsvParserArgs = {}) {
    let delimiter = (args && args.delimiter) || ",";
    if (delimiter instanceof RegExp) {
      delimiter = delimiter.source;
    }
    
    const allElseFinder = /\r?\n|"(\\"|""|[^"])*?"/g;
    this.splitFinder = new RegExp(delimiter + "|" + allElseFinder.source, "g");
  }

  protected get jsonTabSize() {
    if (this.args.jsonTabSize !== undefined) {
      return this.args.jsonTabSize;
    }
    return 2;
  }

  public tryParsePrimitive(parseMe: string): any {
    let convert: string[];
    if (this.args.convertPrimitives === false) { 
      return parseMe; 
    } else if (this.args.convertPrimitives === undefined || this.args.convertPrimitives === true) { 
      convert = ["boolean", "number", "json"]; 
    } else {
      convert = this.args.convertPrimitives as string[];
    }

    try { 
      // start end with [ and ] or { and }
      if (/^\[.*\]$/.test(parseMe) || /^\{.*\}$/.test(parseMe)) {
        if (convert.includes("json") === false) { throw 0; }
        return JSON.parse(parseMe);

      } else if (/^TRUE|true|FALSE|false$/.test(parseMe)) {
        if (convert.includes("boolean") === false) { throw 0; }
        return /true/i.test(parseMe);

      } else if (convert.includes("number") && /^\s*(\d+\.?\d*?|\d*\.\d+)\s*$/.test(parseMe)) {
        let num: number;
        if (/^\s*\d+\s*$/.test(parseMe)) {
          num = parseInt(parseMe);
          if (Number.isSafeInteger(num) === false) {
            return parseMe;
          }
        } else {
          num = parseFloat(parseMe);
        }
        if (isNaN(num)) { throw 0; }
        
        return num;
      }
    } catch(err) {}

    return parseMe;
  }

  /** Turn raw csv text into a 2d array of rows and cells */
  public rawTextTo2dArray(rawCsvText: string) {
    let currentRow: string[] = [];
    if (this.args.trimRawText !== false) {
      rawCsvText = rawCsvText.trim();
    }

    const parseItem = (item: string) => {
      if (item.startsWith("\"")) {
        item = item.replace(/\\"|""/g, "\"");
      }
      item = item.replace(/^"|"$/g, "");
      return this.tryParsePrimitive(item);
    }

    const rowsOut = [currentRow];
    let lastIndex = this.splitFinder.lastIndex = 0;
    let regexResp;
    // tslint:disable-next-line:no-conditional-assignment
    while (regexResp = this.splitFinder.exec(rawCsvText)) {
      const split = regexResp[0];
      if (split.startsWith(`"`) === false) {
        const splitStartIndex = this.splitFinder.lastIndex - split.length;
        const addMe = rawCsvText.substring(lastIndex, splitStartIndex);
        currentRow.push(parseItem(addMe));
        lastIndex = this.splitFinder.lastIndex;
        const isNewLine = /^\r?\n$/.test(split);
        
        if (isNewLine) { rowsOut.push(currentRow = []); }
      }
    }
    const addMe = rawCsvText.slice(lastIndex)
    currentRow.push(parseItem(addMe));
    return rowsOut;
  }

  /** Asynchronously load a file, turn it into 2d array */
  public async fileTo2dArray(filePath: string): Promise<(string | number | object)[][]> {
    const rawCsv = String(await (readFile(filePath)));
    return this.rawTextTo2dArray(rawCsv);
  }


  /** Synchronously load a file, turn it into 2d array */
  public fileTo2dArraySync(filePath: string): (string | number | object)[][] {
    const rawCsv = String(fs.readFileSync(filePath));
    return this.rawTextTo2dArray(rawCsv);
  }


  /** Turn raw csv text into an array of objects */
  public rawTextToObjectArray<T extends {} = any>(rawCsvText: string): T[] {
    const rows = this.rawTextTo2dArray(rawCsvText);
    const propNames = rows.shift() as string[];
    return rows.map((row) => {
      const obj = {} as any;
      let array: any [] | undefined;
      row.forEach((cell, index) => {
        const field = propNames[index];
        if (!field && array) {
          return array.push(cell);
        }
        if (field.startsWith("--array::")) {
          obj[field.replace("--array::", "")] = array = [];
          array.push(cell);
          return;
        }
        array = undefined;
        obj[field] = cell;
      });
      return obj;
    });
  }

  /** Asynchronously load a file, turn it into an array of objects */
  public async fileToObjectArray<T extends {} = any>(filePath: string): Promise<T[]> {
    const rawCsv = String(await (readFile(filePath)));
    return this.rawTextToObjectArray<T>(rawCsv);
  }


  /** Synchronously load a file, turn it into an array of objects */
  public fileToObjectArraySync<T extends {} = any>(filePath: string): T[] {
    const rawCsv = String(fs.readFileSync(filePath));
    return this.rawTextToObjectArray<T>(rawCsv);
  }
  

  /** Turn raw csv text into json text */
  public rawTextToJsonText(rawCsvText: string): string {
    const objectArray = this.rawTextToObjectArray(rawCsvText);
    return JSON.stringify(objectArray, null, this.jsonTabSize);
  }

  /** Asynchronously load a file, convert it to json, then save it 
   * (optional).  You can add a function to parse the object array before saving it
  */
  public async convertFileToJsonFile<T extends {} = any>(
    csvFilePath: string, 
    jsonFilePath: string,
    converter?: (items: T[]) => any[]
  ) {
    let items = this.rawTextToObjectArray(String(await (readFile(csvFilePath))));
    if (converter) {
      items = converter(items);
    }
    const json = JSON.stringify(items, null, this.jsonTabSize);
    return writeFile(jsonFilePath, json);
  }

  /** Synchronously load a file, convert it to json, then save it 
   * (optional).  You can add a function to parse the object array before saving it
  */
  public convertFileToJsonFileSync<T extends {} = any>(
    csvFilePath: string, 
    jsonFilePath: string,
    converter?: (items: T[]) => any[]
  ) {
    let items = this.rawTextToObjectArray(String(fs.readFileSync(csvFilePath)));
    if (converter) {
      items = converter(items);
    }
    const json = JSON.stringify(items, null, this.jsonTabSize);
    return fs.writeFileSync(jsonFilePath, json);
  }
}


const defaultParser = new CsvParser();

/** (default) Turn raw csv text into a 2d array of rows and cells */
export const csvTextTo2dArray = defaultParser.rawTextTo2dArray.bind(defaultParser) as 
  typeof defaultParser.rawTextTo2dArray;

/** (default) Asynchronously load a file, turn it into 2d array */
export const csvFileTo2dArray = defaultParser.fileTo2dArray.bind(defaultParser) as 
  typeof defaultParser.fileTo2dArray;

/** (default) Synchronously load a file, turn it into 2d array */
export const csvFileTo2dArraySync = defaultParser.fileTo2dArraySync.bind(defaultParser) as 
  typeof defaultParser.fileTo2dArraySync;

/** (default) Turn raw csv text into an array of objects */
export const csvTextToObjectArray = defaultParser.rawTextToObjectArray.bind(defaultParser) as 
  typeof defaultParser.rawTextToObjectArray;

/** (default) Asynchronously load a file, turn it into an array of objects */
export const csvFileToObjectArray = defaultParser.fileToObjectArray.bind(defaultParser) as
  typeof defaultParser.fileToObjectArray;

/** (default) Synchronously load a file, turn it into an array of objects */
export const csvFileToObjectArraySync = defaultParser.fileToObjectArraySync.bind(defaultParser) as
  typeof defaultParser.fileToObjectArraySync;

/** (default) Turn raw csv text into json text */
export const csvTextToJsonText = defaultParser.rawTextToJsonText.bind(defaultParser) as
  typeof defaultParser.rawTextToJsonText;

/** (default) Asynchronously load a file, convert it to json, then save it */
export const csvFileToJsonFile = defaultParser.convertFileToJsonFile.bind(defaultParser) as
  typeof defaultParser.convertFileToJsonFile;

/** (default) Synchronously load a file, convert it to json, then save it */
export const csvFileToJsonFileSync = defaultParser.convertFileToJsonFileSync.bind(defaultParser) as
  typeof defaultParser.convertFileToJsonFileSync;