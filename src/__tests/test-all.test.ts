import test from "ava";
import { join } from "path";
import { CsvParser, csvTextTo2dArray } from "../index";

const testText = `
a,b,c
1,2,3
"list, of, things","a ""tough, ugly, quote""",thing
TRUE,bar,qux
"[1, 2, 3]","{""first"": 1}",false
`

test(`Doesn't break on quotes or commas.  Handles primitives and json.`, async (t) => {
  const array2d = csvTextTo2dArray(testText);
  t.deepEqual(array2d, [
    ["a", "b", "c"],
    [1, 2, 3],
    ["list, of, things", `a "tough, ugly, quote"`, `thing`],
    [true, "bar", `qux`],
    [[1,2,3], {first: 1}, false],
  ])
})

test("Loads file, doesn't break on quotes or commas", async (t) => {
  const parser = new CsvParser();
  const items = await parser.fileToObjectArray(join(__dirname, "./test.csv"));
  t.deepEqual(items, [
    {col1: `a, b, "c", d`, col2: "e f"},
    {col1: "foo", col2: "bar"}
  ])
});



test("Can do tsv", async (t) => {
  const parser = new CsvParser({
    delimiter: /\t/
  });
  const items = parser.rawTextToObjectArray(`col1\tcol2\na\tb\n1\t2`);
  t.deepEqual(items, [
    {col1: "a", col2: "b"},
    {col1: 1, col2: 2},
  ])
});