# actual-csv-to-json-parser

This tool converts from CSV to Object arrays, or JSON.  It can do TSV too.  Here's a super basic example.

1. Install

    `npm i actual-csv-to-json-parser`

2. Call Function

    ```ts
    import { csvTextToObjectArray } from "actual-csv-to-json-parser";

    const numbers = csvTextToObjectArray(`
      name, num, even
      ten, 10, TRUE
      fifteen, 15, FALSE
    `);

    console.log(numbers[0]) // --> { name: "ten", num: 10, even: true }
    console.log(numbers[1]) // --> { name: "fifteen", num: 15, even: false }
    ```

    --alternatively--

    ```ts
    import { csvFileToObjectArraySync } from "actual-csv-to-json-parser";
    import * as pathTool from "path";

    const csvLoadPath = pathTool.join(__dirname, "/path/to/file.csv");
    const items = csvFileToObjectArraySync(csvLoadPath);
    items.forEach((item) => console.log(item));
    ```

    --or as an extreme example--
    ```ts
    import { CsvParser } from "actual-csv-to-json-parser";
    import * as pathTool from "path";

    const csvLoadPath = pathTool.join(__dirname, "/path/to/file.csv");
    const jsonSavePath = pathTool.join(__dirname, "/path/to/output.json");

    // all the bells and whistles
    const parser = new CsvParser({
      delimiter: /,/,     // default
      jsonTabSize: 2,     // default
      trimRawText: true,  // default
      convertPrimitives: ["boolean", "number", "array", "object"],  // default
    });

    // load a csv file, modify it, save it as json
    parser.convertFileToJsonFileSync<{name: string, age: number}>(csvLoadPath, jsonSavePath, (items) => {
      items.forEach((item) => item.age += 1);
      return items;
    })
    ```

*See "Overview of Functions" for more*

<br><br>

------

## Features

- Simple one liner functions
- Turns CSV into 2d String Array, JSON, or Object Array
- Handles quotes and commas just fine
- Can parse primitives (number, string, array, boolean, object)
- Can take raw CSV text
- Can do synchronous and asynchronous file loading
- Can set special delimiters (ie tabs for TSV)
- Typescript
- 150 lines
- That's it.  

If you want a typescript interface of your json object, check out this tool: http://json2ts.com/
<br><br>

------

## Overview of Functions

More later, but this is the basics of what exists

*Default quick functions*
- Creating Object Arrays
  - `csvTextToObjectArray(rawCsvText)`
  - `async csvFileToObjectArray(csvFilePath)`
  - `csvFileToObjectArraySync(csvFilePath)`
- Creating 2D String Arrays
  - `csvTextTo2dArray(rawCsvText)`
  - `async csvFileTo2dArray(csvFilePath)`
  - `csvFileTo2dArraySync(csvFilePath)`
- Creating JSON strings/files
  - `csvTextToJsonText(rawCsvText)`
  - `async csvFileToJsonFile(csvFilePath, jsonSavePath)`
  - `csvFileToJsonFileSync(csvFilePath, jsonSavePath)`

*Custom specialty stuff.  Same as above but with customized csv parser args*

- ICsvParserArgs { delimiter, jsonTabSize }
- `new CsvParser(customizationArgs)`
  - `this.rawTextTo2dArray(rawCsvText)`
  - `this.fileTo2dArray(csvFilePath)`
  - `this.fileTo2dArraySync(csvFilePath)`
  - `this.rawTextToObjectArray(rawCsvText)`
  - `this.fileToObjectArray(csvFilePath)`
  - `this.fileToObjectArraySync(csvFilePath)`
  - `this.rawTextToJsonText(rawCsvText)`
  - `this.convertFileToJsonFile(csvFilePath, jsonSavePath)`
  - `this.convertFileToJsonFileSync(csvFilePath, jsonSavePath)`

<br><br>

-------

## What's a CSV or JSON or Object Array?

- **This is a CSV (Comma Separated Values):**

  ```
  name, age, eyesight
  bob, 21, 20/20
  julie, 27, 21/19
  mark, 42, 17/17
  ```

- **This is an 2D String Array**
  ```ts
  const peeps = [
    [ "name", "age", "eyesight" ],
    [ "bob", "21", "20/20" ],
    [ "julie", "27", "21/19" ],
    [ "mark", "42", "17/17" ],
  ]
  ```

- **This is an object array (probably what you want)**
  ```ts
  const peeps = [
    { name: "bob", age: "21", eyesight: "20/20"},
    { name: "julie", age: "27", eyesight: "21/19"},
    { name: "mark", age: "42", eyesight: "17/17"},
  ]
  ```

- **This is json (JSON is a string)**
  ```ts
  const json = `[
    {
      "name": "bob",
      "age": "21",
      "eyesight": "20/20"
    },
    {
      "name": "julie",
      "age": "27",
      "eyesight": "21/19"
    },
    {
      "name": "mark",
      "age": "42",
      "eyesight": "17/17"
    },
  ]`
  ```

<br><br>

---------

## Documentation

### If you can read typescript, just [look at the source code](https://gitlab.com/SephReed/actual-csv-to-json-parser/-/blob/master/src/index.ts).  It's a single ~250 line file.

<br>

*This data is used in the examples below*

```ts
// imagine raw text also found at /path/to/file.csv
const EXAMPLE_DATA = `
a, b
1, 2
x, y
`;

interface IExampleData {
  a: string;
  b: string;
}
```
<br>

**Creating Object Arrays**

- **`csvTextToObjectArray(rawCsvText: string)`**

  Creates an object array from csv.  Most useful function, likely.  The `IExampleData` templating is optional.
  ```ts
  const items = csvTextToObjectArray<IExampleData>(EXAMPLE_DATA);
  /* items === [
    {a: "1", b: "2"},
    {a: "x", b: "y"}
  ]*/
  ```
  <br>

- **`csvFileToObjectArray(csvFilePath: string)`**

  Asynchronously load a file, turn it into an array of objects.  The `IExampleData` templating is optional.
  ```ts
  const csvLoadPath = path.join(__dirname, "/path/to/file.csv");
  csvFileToObjectArray<IExampleData>(csvLoadPath).then((items) => console.log(items));
  // or
  const items = await csvFileToObjectArray<IExampleData>(csvLoadPath);
  ```
  <br>

- **`csvFileToObjectArraySync(csvFilePath: string)`**

  Synchronously load a file, turn it into an array of objects. The `IExampleData` templating is optional.
  ```ts
  const csvLoadPath = path.join(__dirname, "/path/to/file.csv");
  const items = csvFileToObjectArraySync<IExampleData>(csvLoadPath);
  ```

<br><br><br>


**Creating 2D String Arrays**

- **`csvTextTo2dArray(rawCsvText: string)`**

  Creates a 2d array from raw csv text
  ```ts
  const array2d = csvTextTo2dArray(EXAMPLE_DATA);
  /* array2d === [
    ["a", "b"],
    ["1", "2"],
    ["x", "y"],
  ]*/
  ```
  <br>

- **`csvFileTo2dArray(csvFilePath: string)`**

  Asynchronously creates a 2d array from a csv file
  ```ts
  const csvLoadPath = path.join(__dirname, "/path/to/file.csv");
  csvFileTo2dArray(csvLoadPath).then((array2d) => console.log(array2d));
  // or
  const array2d = await csvFileTo2dArray(csvLoadPath);
  ```
  <br>

- **`csvFileTo2dArraySync(csvFilePath: string)`**

  Synchronously creates a 2d array from a csv file
  ```ts
  const csvLoadPath = path.join(__dirname, "/path/to/file.csv");
  const array2d = csvFileTo2dArraySync(csvLoadPath);
  ```
<br><br><br>

**Creating JSON string**

- **`csvTextToJsonText(rawCsvText: string)`**

  Turn raw csv text into json text
  ```ts
  const json = csvTextToJsonText(EXAMPLE_DATA);
  /* json === `[
    {"a": "1", "b": "2"},
    {"a": "x", "b": "y"}
  ]`*/
  ```

  <br><br><br>

**Direct conversion from .csv to .json files**

- **`csvFileToJsonFile(csvFilePath: string, jsonSavePath: string)`**

  Asynchronously load a file, convert it to json, then save it.  The `IExampleData` templating is optional.
  (optional).  You can add a function to parse the object array before saving it
  ```ts
  const csvLoadPath = path.join(__dirname, "/path/to/file.csv");
  const jsonSavePath = path.join(__dirname, "/path/to/output.json")
  csvFileToJsonFile(csvLoadPath, jsonSavePath).then(() => console.log("File saved"))

  // or (with conversion)
  await csvFileToJsonFile<IExampleData>(csvLoadPath, jsonSavePath, (items) => {
    items.forEach((it) => it.b = "foo");
    return items;
  });
  ```
  <br>

- **`csvFileToJsonFileSync(csvFilePath: string, jsonSavePath: string)`**

  Synchronously load a file, convert it to json, then save it.  The `IExampleData` templating is optional.
  (optional).  You can add a function to parse the object array before saving it
  ```ts
  const csvLoadPath = path.join(__dirname, "/path/to/file.csv");
  const jsonSavePath = path.join(__dirname, "/path/to/output.json")
  csvFileToJsonFileSync(csvLoadPath, jsonSavePath);

  // or (with conversion)
  csvFileToJsonFileSync<IExampleData>(csvLoadPath, jsonSavePath, (items) => {
    items.forEach((it) => it.b = "foo");
    return items;
  });
  ```


<br><br><br>

**Special Cases**

- **`ICsvParserArgs`**

  All the special setting for the parser.
  ```ts
  interface ICsvParserArgs {
    delimiter?: RegExp | string;
    jsonTabSize?: null | number;
    trimRawText?: boolean;
    convertPrimitives?: boolean | Array<"boolean" | "number" | "array" | "object">,
  }
  ```

- **`new CsvParser(args: ICsvParserArgs)`**

  Create a special csv parser, with custom rules
  ```ts
  const parser = new CsvParser({
    delimeter: "\t",    // now does TSV
    jsonTabSize: 3,     // set to null for one-liner json
  });

  const array2d = parser.fileTo2dArraySync("/path/to/file.csv");
  ```

<br><br>

-----

## TSV

Search for `CsvParser()`.  It has a delimiter argument.


<br><br>

------

## One of the Tests

```ts
const testText = `
a,b,c
1,2,3
"list, of, things","a ""tough, ugly, quote""",thing
TRUE,bar,qux
"[1, 2, 3]","{""first"": 1}",false
`

test(`Doesn't break on quotes or commas.  Handles primitives and json.`, async (t) => {
  const array2d = csvTextTo2dArray(testText);
  t.deepEqual(array2d, [
    ["a", "b", "c"],
    [1, 2, 3],
    ["list, of, things", `a "tough, ugly, quote"`, `thing`],
    [true, "bar", `qux`],
    [[1,2,3], {first: 1}, false],
  ])
})
```

<br><br>

---------

## Rant

This isn't so much a rant, I'm just going to say I didn't much like the other csv tools.  Here's the best two I could find:

- [csvtojson](https://www.npmjs.com/package/csvtojson) 
  - Unpacked Size 8.69 MB, Total Files 393, Dependencies 3.  
  - Does way too much, difficult to figure how to do simple things.
  - Is cool once you figure it out, I think.

- [papaparse](https://www.npmjs.com/package/papaparse)
  - Unpacked Size 249kB, Total Files 21, Dependencies 0. 👍
  - First feature is "Easy to use"
  - Doesn't show an example.
  - Doesn't seem to have typescript typings... does it do promises or callbacks?
  - Probably good, but the npm docs just aren't doing it justice..

Then there's like [100 more](https://www.npmjs.com/search?q=csv%20to%20json).