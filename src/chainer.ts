

export type CsvJsonTool<T extends Object = any> = {
  fetch: (url: string) => CsvJsonParser<T, Promise<string[][]>, Promise<T[]>, Promise<string>>;
  loadAsync: (filepath: string) => CsvJsonParser<T, Promise<string[][]>, Promise<T[]>, Promise<string>>;
  loadSync: (filepath: string) => CsvJsonParser<T>;
  rawText: (rawCsv: string) => CsvJsonParser<T>;
}

export type ValueParseType = "auto" | "string" | "date" | "number" | "boolean" | ((value: string) => any);
export type ValueTypesSpecified<T extends Object> = Partial<Record<keyof T, ValueParseType>>;

export type ReplaceHeadersArg<T extends Object> = Array<[string, keyof T]>;
export type TrimStringsArg = "pre-parse" | "post-parse";


export type CsvJsonParser<
  T extends Object,
  TWO_D extends string[][] | Promise<string[][]> = string[][],
  OBJ_ARR extends T[] | Promise<T[]> = T[],
  JSON extends string | Promise<string> = string,
> = {
  to2dArray():  TWO_D;
  toObject(): OBJ_ARR;
  toJson(arg?: { tabSize?: number }): JSON;
  valueTypes: (arg: ValueTypesSpecified<T> | ValueParseType) => CsvJsonParser<T, TWO_D, OBJ_ARR, JSON>;
  replaceHeaders: (arg: ReplaceHeadersArg<T>) => CsvJsonParser<T, TWO_D, OBJ_ARR, JSON>;
  trimStrings: (arg?: TrimStringsArg) => CsvJsonParser<T, TWO_D, OBJ_ARR, JSON>;
  delimiter: (arg:  RegExp | string) => CsvJsonParser<T, TWO_D, OBJ_ARR, JSON>;
}


export type LoadType = "fetch" | "loadAsync" | "loadSync" | "rawText";
export type ParseArgs<T extends Object> = {
  loadType: LoadType;
  valueTypes: ValueTypesSpecified<T> | ValueParseType;
  replaceHeaders: ReplaceHeadersArg<T>;
  trimStrings: TrimStringsArg;
  delimiter?:  RegExp | string;
}




export class _CsvTool {
  constructor(protected args: ParseArgs) {

  }
}





// export class _CsvTool {
//   public get fetch<T extends Object = any>() {
//     return this.generateChainer("fetch") as CsvJsonParser<T, Promise<string[][]>, Promise<T[]>, Promise<string>>;
//   }

//   protected generateChainer(type: "fetch" | "loadAsync" | "loadSync" | "rawText") {
//     return null;
//   }
// }

// export const CsvTool = new _CsvTool();
